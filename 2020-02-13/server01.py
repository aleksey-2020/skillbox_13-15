import datetime
import time as ti
from flask import Flask, request, abort, jsonify

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello, World!"

@app.route("/status", methods=['GET', 'POST'])
def status():
    return {
        "status": True,
        "time": datetime.datetime.now()
    }

@app.route("/time", methods=['GET', 'POST'])
def time():
    return {
        "time": ti.ctime()
    }

@app.route("/status-test", methods=['GET', 'POST'])
def status_test():
    data = request.json['test']
    return {
        "status": True,
        "req": data,
        "time": datetime.datetime.now()
    }

@app.errorhandler(404)
def page_not_found(error):
    return 'Error 404: page not found' ,404

@app.errorhandler(500)
def page_not_found(error):
    return 'Error 500: bad request' ,500

app.run(host='127.0.0.1', port=8888)

