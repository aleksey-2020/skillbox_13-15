import random

print( [random.randint(11, 1020) for _ in range(20)] )

ages = [
    {'name': f"Name {i}", 'soname': f'soname {i}', 'age': random.randint(11, 1020)}
    for i in range(20)
]

print(ages)
