import requests
import json

print (
    json.loads(
        requests.post("http://127.0.0.1:8888/time").text
    )['time']
)

print (
    json.loads(
        requests.post("http://127.0.0.1:8888/status").text
    )['status']
)

print (
    json.loads(
        requests.get( "http://127.0.0.1:8888/status-test", json={"test":"test2"}).text
    )['status']
)

print (
    json.loads(
        requests.post( "http://127.0.0.1:8888/status-test", json={"test":"test2"}).text
    )['req']
)

request=requests.get("http://127.0.0.1:8888/time")
print(request)
