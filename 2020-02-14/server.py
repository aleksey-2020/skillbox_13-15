# 1. Передача сервер-клиент с использованием TLS

import sys
import time
from datetime import datetime
from flask import Flask, request
import ssl

scriptDir = sys.path[0]           # папка где запускается скрипт

sslContext = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
sslContext.load_cert_chain(
        f'{scriptDir}/server.crt', # openssl req -new -x509 -nodes -sha256 -days 365 -key server.key -out server.crt
        f'{scriptDir}/server.key', # openssl genrsa 2048 > server.key

    )

app = Flask(__name__)
messages = [
    {'username': 'jack', 'text': 'Hello', 'time': time.time()},
    {'username': 'mary', 'text': 'Hi, jack!', 'time': time.time()},
]
users = {
    # username: password
    'jack': 'black',
    'mary': '12345',
}


@app.route("/")
def hello():
    return "Hello, World!"


@app.route("/status")
def status():
    return {
        'status': True,
        'time': datetime.now().strftime('%Y/%m/%d %H:%M:%S'),
        'total users': len(users),
        'total messages': len(messages),
    }


@app.route("/history")
def history():
    """
    request: ?after=1234567890.4567
    response: {
        "messages": [
            {"username": "str", "text": "str", "time": float},
            ...
        ]
    }
    """
    after = float(request.args['after'])

    # filtered_messages = []
    # for message in messages:
    #     if after < message['time']:
    #         filtered_messages.append(message)

    filtered_messages = [m for m in messages if after < m['time']]

    return {'messages': filtered_messages}


@app.route("/send", methods=['POST'])
def send():
    """
    request: {"username": "str", "password": "str", "text": "str"}
    response: {"ok": true}
    """
    data = request.json  # JSON -> dict
    username = data['username']
    password = data['password']
    text = data['text']

    # если такой пользователь существует -> проверим пароль
    # иначе мы зарегистрируем его
    if username in users:
        real_password = users[username]
        if real_password != password:
            return {"ok": False}
    else:
        users[username] = password

    new_message = {'username': username, 'text': text, 'time': time.time()}
    messages.append(new_message)

    return {"ok": True}


app.run(host='127.0.0.1', port=5000, debug=True, ssl_context=sslContext)
