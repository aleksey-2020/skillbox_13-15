from datetime import datetime
from time import sleep
import sys

import requests

# {
#     "messages": [
#         {"username": "str", "text": "str", "time": float},
#         ...
#     ]
# }

scriptDir = sys.path[0]           # папка где запускается скрипт

last_message_time = 0
while True:
    response = requests.get(
        'https://127.0.0.1:5000/history',
        verify=False,
        params={'after': last_message_time}
    )
    data = response.json()
    for message in data['messages']:
        # float -> datetime
        beauty_time = datetime.fromtimestamp(message['time'])
        beauty_time = beauty_time.strftime('%Y/%m/%d %H:%M:%S')
        print(beauty_time, message['username'])
        print(message['text'])
        print()
        last_message_time = message['time']

    sleep(1)
